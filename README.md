## Label Free Quantitative Proteomics (MaxQuant and Persues) - AWS Windows Instance

The following workflow is intended to help our collaborators (and ourselves!) in analysis of LFQ proteomics data via MaxQuant using AWS Cloud at the Emory Integrated Computing Core (EICC).

### Step1: Sign-up for AWS account and get access and secret keys

Access key ID:

Secret access key:

### Step 2: Login and create a (or use an existing) security group, set inbound rules and download the key

a). Login URL: https://eicc-aws.signin.aws.amazon.com/console

`UserID:`

`Password:`

Get a public DNS, if not selected while Windows Instance creation
 
Public DNS (IPv4)

* Go to https://eicc-aws.signin.aws.amazon.com/console
* Go To Services -> VPC
* Open Your VPCs
* Select your VPC connected to your EC2 and
* Select Actions => Edit DNS Hostnames ---> Change DNS hostnames: to YES

b). Create a “Security Group”.Security group acts like your cloud firewall to protect your applications and data. The right combination of VPC, Network Access Control Lists (NACLs), and Security Groups allows you to create a secure perimeter around your AWS resources.

Open security group: Edit Inbound 

Source: change it to MyIP or Anywhere

| Type | Protocol | Port Range |
| ---- |:--------:| ----------:|
| SSH  | TCP      | 22         |	
| RDP  | TCP      | 3389       |

Key Pairs. Key pairs are unrelated to access keys, and consist of a public key and a private key. You use the private key to create a digital signature, and then AWS uses the corresponding public key to validate the signature. Key pairs are used only for Amazon EC2 and Amazon CloudFront.

[ec2-create-your-key-pair](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair)

Download the key `eicc.pem` to local computer and set the permissions of your private key file so that only you can read it!

`chmod 400 eicc.pem`

### Step3: Install and configure `aws cli` on your local computer

[Install aws cli](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

`$aws configure`

`AWS Access Key ID [****************YU6Q]:`

`AWS Secret Access Key [****************peMR]:`

`Default region name [us-east-1b]:` 

`Default output format [json]:` 

The above commands create a directory called `.aws` with files `credentials` and `config`.

Check your file at `~/.aws/config`, you have something like

`[default]`

`region=us-east-1a`

Fix the region to `region=us-east-1` and then the `aws` command will work correctly.

### Step 4: Create S3 bucket (eg “proteomics”) and copy your raw data using AWS Client

a). Login and create a bucket using [URL](https://eicc-aws.signin.aws.amazon.com/console)

b). At AWS Clent, the following `ls` command lists all of the buckets owned by the user.

`aws s3 ls`

`aws s3 ls s3://proteomics`

To transfer data to and from S3 [commands](https://docs.aws.amazon.com/cli/latest/userguide/using-s3-commands.html)

`aws s3 cp /Volumes/Untitled/control s3://proteomics/control`

### Step 5: Download Remote desktop (RDP) application and access a windows instance using RDP.

Public DNS: `ec2-******.compute-1.amazonaws.com`

User name: `Administrator`

### Step 6: Format the disk at Windows instance

Search `disk` and click Create and format hard disk partitions. 

Enter the created drive eg. `D:`

`C:\> D:` (then enter)

To copy S3 data to Windows instance, install windows based AWS CLI at Windows Instance and use the same access and secret keys used at your local computer.

`aws_access_key_id =`

`aws_secret_access_key =`

Now at Windows CMD, use `aws s3 ls` to see all S3 buckets.

`aws s3 cp s3://proteomics/ data/ --recursive`

### Step 7: Register, download and install MaxQuant and Persues

`MaxQuant.Registration code:`

To Download MaxQuant, please follow this [link](https://lotus1.gwdg.de/mpg/mmbc/maxquant_input.nsf/download?OpenFrameset)

`Perseus Registration code:`

To Download Perseus, please follow this [link](https://lotus1.gwdg.de/mpg/mmbc/mann/perseus_input.nsf/download?OpenFrameset)
